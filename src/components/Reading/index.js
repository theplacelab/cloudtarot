import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Reading.module.css';
import {
  faUndo,
  faLink,
  faArrowRight,
  faArrowLeft
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';
import jwt from 'jwt-simple';
import util from 'util.js';
import memoizeOne from 'memoize-one';

class Reading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCardIndex: 0,
      currentCard: this.props.selectedDeck[0]
    };
    this.m_setBackground = memoizeOne((el, currentCard) => {
      if (!currentCard || !el) return;
      el.style.background = `url(${util.makeAssetURL(
        `${process.env.REACT_APP_CONFIG}/images/${currentCard.file}`
      )}) center center no-repeat`;
    });
  }

  onSave = () => {
    const token = jwt.encode(
      this.props.selectedDeck.map(card => {
        return { id: card.id, inverted: card.inverted };
      }),
      this.props.version
    );
    this.props.onSave(token);
  };

  componentDidMount() {
    this.m_setBackground(
      document.getElementById('currentCard'),
      this.props.selectedDeck[this.state.currentCardIndex]
    );
  }

  render() {
    this.m_setBackground(
      document.getElementById('currentCard'),
      this.props.selectedDeck[this.state.currentCardIndex]
    );
    let currentCard = this.props.selectedDeck[this.state.currentCardIndex];
    let nextVisible =
      this.state.currentCardIndex <
      this.props.deck.config[this.props.readingType].drawCount - 1;
    let prevVisible = this.state.currentCardIndex > 0;
    let showArrows = nextVisible || prevVisible;
    return (
      <React.Fragment>
        <div className={styles.instructions}>
          <div className={styles.navbuttons}>
            <div className={styles.navbuttons_arrows}>
              {showArrows && (
                <React.Fragment>
                  <FontAwesomeIcon
                    className={`icon ${prevVisible ? '' : styles.disabled}`}
                    icon={faArrowLeft}
                    onClick={this.goPrevious}
                  />
                  <FontAwesomeIcon
                    className={`icon ${nextVisible ? '' : styles.disabled}`}
                    icon={faArrowRight}
                    onClick={this.goNext}
                  />
                </React.Fragment>
              )}
            </div>
            <div className={styles.foo}>
              <div onClick={this.props.onReset}>
                <FontAwesomeIcon className={'icon'} icon={faUndo} />
              </div>

              <div onClick={this.onSave}>
                <FontAwesomeIcon className={'icon'} icon={faLink} />
              </div>
            </div>
          </div>
          <div className={showArrows ? styles.title : styles.title_noarrows}>
            {
              this.props.deck.config[this.props.readingType].position[
                this.state.currentCardIndex
              ]
            }
            :{' '}
            {currentCard.inverted
              ? `${currentCard.name} Inverted`
              : currentCard.name}
          </div>
        </div>

        <div className={styles.currentCard_container}>
          <div className={styles.currentCard} id="currentCard">
            <div className={styles.currentCard_desc}>
              {currentCard.inverted
                ? currentCard.reading.inverted
                : currentCard.reading.upright}{' '}
            </div>
            <div
              className={
                currentCard.inverted
                  ? `${styles.currentCard_card} ${styles.isInverted}`
                  : `${styles.currentCard_card}`
              }
            >
              <img
                alt={currentCard.name}
                src={util.makeAssetURL(
                  `${process.env.REACT_APP_CONFIG}/images/${currentCard.file}`
                )}
              />
            </div>
          </div>
          <div className={`${styles.cards} ${styles[this.props.readingType]}`}>
            {this.props.selectedDeck.map((card, idx) => {
              return (
                <Card
                  disabled={false}
                  id={card.name}
                  onSelect={this.onSelect}
                  key={idx}
                  isFlipped={idx > this.state.currentCardIndex}
                  isInverted={card.inverted}
                  isSelected={false}
                  name={card.name}
                  reading={card.config}
                  imageURL={card.file}
                />
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }

  goPrevious = () => {
    if (this.state.currentCardIndex > 0)
      this.setState({ currentCardIndex: this.state.currentCardIndex - 1 });
  };

  goNext = () => {
    if (
      this.state.currentCardIndex <
      this.props.deck.config[this.props.readingType].drawCount - 1
    )
      this.setState({ currentCardIndex: this.state.currentCardIndex + 1 });
  };
}
export default Reading;

Reading.propTypes = {
  readingType: PropTypes.string.isRequired,
  selectedDeck: PropTypes.array,
  deck: PropTypes.object.isRequired,
  cards: PropTypes.array,
  version: PropTypes.string,
  remainingCount: PropTypes.number,
  currentCard: PropTypes.array,
  onReset: PropTypes.func,
  onSave: PropTypes.func
};

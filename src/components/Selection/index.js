import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Selection.module.css';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import util from 'util.js';

class Selection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shuffledCards: util.shuffle(this.props.cards)
    };
  }

  render() {
    return (
      <React.Fragment>
        <div className={styles.instructions}>
          <div className={styles.remainingCount}>
            {this.props.remainingCount !== 0 && (
              <div className={this.props.remainingCount}>
                Please select{' '}
                <span className={styles.remainingCount_number}>
                  {this.props.remainingCount}
                </span>{' '}
                {this.props.remainingCount === 1 ? 'card' : 'cards'}
              </div>
            )}
          </div>
          <div>
            <div onClick={this.props.onReset}>
              <FontAwesomeIcon className={'icon'} icon={faUndo} />
            </div>
          </div>
        </div>

        <div className={styles.cards}>
          {this.state.shuffledCards?.map((card, idx) => {
            return (
              <Card
                disabled={false}
                id={card.name}
                onSelect={() => this.props.onSelect(card)}
                key={idx}
                isFlippable={false}
                isFlipped={true}
                isInverted={card.inverted}
                isSelected={
                  this.props.selectedDeck.find(
                    thisCard => thisCard.id === card.id
                  )
                    ? true
                    : false
                }
                name={card.name}
                reading={card.config}
                imageURL={card.file}
              />
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default Selection;

Selection.propTypes = {
  selectedDeck: PropTypes.array,
  readingType: PropTypes.string,
  deck: PropTypes.object.isRequired,
  cards: PropTypes.array,
  remainingCount: PropTypes.number,
  currentCard: PropTypes.number,
  onSelect: PropTypes.func,
  onReset: PropTypes.func
};

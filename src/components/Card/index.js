import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.css';
import util from 'util.js';

class Card extends Component {
  render() {
    let classname = this.props.isInverted
      ? `${this.props.isSelected ? styles.isSelected : ''} ${styles.card} ${
          styles.isInverted
        }`
      : `${this.props.isSelected ? styles.isSelected : ''} ${styles.card}`;

    let cardClassName = this.props.isFlipped
      ? `${styles.cardInner} ${styles.isFlipped}`
      : styles.cardInner;

    return (
      <div
        onClick={this.selectCard}
        onContextMenu={e => {
          e.preventDefault();
        }}
        className={`card ${classname}`}
      >
        <div className={cardClassName}>
          <div className={styles.cardBack}>
            <img
              src={util.makeAssetURL(
                `${process.env.REACT_APP_CONFIG}/images/_back.png`
              )}
              alt="Back of Card"
            />
          </div>
          {this.props.isFlippable && (
            <div className={styles.cardFront}>
              <img
                src={util.makeAssetURL(
                  `${process.env.REACT_APP_CONFIG}/images/${this.props.imageURL}`
                )}
                alt="Card"
              />
            </div>
          )}
        </div>
      </div>
    );
  }

  flipCard = e => {
    e.currentTarget.firstElementChild.classList.toggle(styles.isFlipped);
    console.log(e.currentTarget.firstElementChild);
  };

  selectCard = () => {
    if (!this.props.disabled && typeof this.props.onSelect === 'function')
      this.props.onSelect(this.props.id);
  };

  describeCard = () => {
    let reading = this.props.isInverted
      ? this.props.config.isInverted
      : this.props.config.upright;

    window.alert(
      `${this.props.name} ${
        this.props.isInverted ? '(Inverted)' : ''
      }${reading}`
    );
  };
}
export default Card;

Card.defaultProps = {
  isSelected: false,
  isInverted: false,
  disabled: false,
  isFlipped: true,
  isFlippable: true
};
Card.propTypes = {
  isFlippable: PropTypes.bool,
  isFlipped: PropTypes.bool,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  onSelect: PropTypes.func,
  isSelected: PropTypes.bool,
  imageURL: PropTypes.string,
  name: PropTypes.string,
  isInverted: PropTypes.bool,
  config: PropTypes.object
};

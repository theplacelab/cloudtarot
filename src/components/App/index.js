import React, { Component } from 'react';
import styles from './App.module.css';
import AlertBar from 'components/AlertBar';
import jwt from 'jwt-simple';
import Selection from 'components/Selection';
import Reading from 'components/Reading';
import util from 'util.js';
import memoizeOne from 'memoize-one';
import PropTypes from 'prop-types';
import Spinner from 'components/Spinner';

class App extends Component {
  constructor(props) {
    super(props);

    this.defaultState = {
      initialized: false,
      readingType: this.props.readingType,
      currentCardIndex: 0,
      selectedDeck: [],
      statusBar: {
        text: ''
      }
    };
    this.state = { ...this.defaultState };
    fetch(`${process.env.REACT_APP_CONFIG}/index.json`)
      .then(response => {
        return response.json();
      })
      .then(deck => {
        let allCards = deck.major.cards;
        Object.keys(deck.suits).forEach(suit => {
          deck.suits[suit].cards.forEach(card => {
            allCards.push(card);
          });
        });
        this.setState({
          deck,
          allCards,
          initialized: true
        });
      });

    window.addEventListener('load', () => {
      document.body.style.transition = '0.7s opacity';
      document.body.style.opacity = 1;
    });

    this.m_unpackToken = memoizeOne(() => {
      let token = window.location.href.split('/')[
        window.location.href.split('/').length - 1
      ];
      try {
        const decoded = jwt.decode(token, this.state.deck.version);

        let selectedDeck = [];
        decoded.forEach(item =>
          selectedDeck.push({
            ...this.state.allCards.find(card => card.id === item.id),
            inverted: item.inverted
          })
        );
        return selectedDeck;
      } catch (e) {
        if (token.length > 0) window.alert('Sorry that reading is expired!');
      }
      return null;
    });
  }

  onReset = () => {
    let url = window.location.href;
    let base = url.split('/')[2];
    window.location = `http://${base}`;
  };

  onSelect = card => {
    let selectedDeck = [...this.state.selectedDeck];
    selectedDeck.push(card);
    this.setState({ selectedDeck });
  };

  render() {
    if (!this.state.initialized) return <Spinner />;

    let selectedDeck = this.m_unpackToken(window.location.href);
    selectedDeck = selectedDeck ? selectedDeck : this.state.selectedDeck;

    let remainingCount =
      this.state.deck.config[this.state.readingType].drawCount -
      selectedDeck.length;

    return (
      <div className={styles.container}>
        <AlertBar />
        {(remainingCount !== 0 && (
          <Selection
            onReset={this.onReset}
            onSelect={this.onSelect}
            remainingCount={remainingCount}
            readingType={this.state.readingType}
            selectedDeck={selectedDeck}
            deck={this.state.deck}
            cards={this.state.allCards}
          />
        )) || (
          <Reading
            version={this.state.deck.version}
            readingType={this.state.readingType}
            onSave={util.copyUrlToClipboard}
            onReset={this.onReset}
            currentCardIndex={this.state.currentCardIndex}
            selectedDeck={selectedDeck}
            remainingCount={remainingCount}
            deck={this.state.deck}
            cards={this.state.allCards}
          />
        )}
      </div>
    );
  }
}

export default App;

App.defaultProps = {
  readingType: 'celtic'
};

App.propTypes = {
  readingType: PropTypes.string
};

const util = {
  shuffle: deck => {
    //https://wsvincent.com/javascript-object-oriented-deck-cards/
    let m = deck.length;
    let i;
    while (m) {
      i = Math.floor(Math.random() * m--);
      deck[i].inverted = Math.floor(Math.random() * 2) === 0 ? false : true;
      [deck[m], deck[i]] = [deck[i], deck[m]];
    }
    return deck;
  },

  makeAssetURL: url => {
    if (!url || url.includes('http')) {
      return url;
    } else {
      return `${process.env.REACT_APP_CONFIG}/${url}`;
    }
  },

  copyUrlToClipboard: token => {
    let url = window.location.href;
    let base = url.split('/')[2];

    const text = `http://${base}/${token}`;
    navigator.clipboard.writeText(text).then(
      () => {
        window.alertBar('Copied link to clipboard.');
      },
      function(err) {
        console.error('Could not copy text: ', err);
      }
    );
  }
};
export default util;

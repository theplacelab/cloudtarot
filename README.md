# CloudTarot
**[Version: 0.0.1](https://semver.org/)**  
Code formatted with [Prettier](https://prettier.io/)  
2020 **[Place Lab Ltd.](https://theplacelab.com)** - MIT Licensed  
a@feralresearch.org / Twitter: [@tezcatlipoca](http://twitter.com/tezcatlipoca)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/K3K2IKEJ)

This is a standalone React app which provides basic oracle-card functionality. Out of the box it supports Celtic cross, three and one-card spreads. It also provides a "linking" functionality so that you can save a link to your reading for sharing. 

You can see examples of it in action at [cloudTarot.com](https://CloudTarot.com)

The license does not require you to tell us what you do with this software, but we are very curious! Please contact us if you're enjoying the project.

--- 

## The Default Card Set
The code includes a default card set. The images and the text are from the [1909 Rider-Waite deck](https://en.wikipedia.org/wiki/Rider-Waite_tarot_deck) (now in the public domain) as found on Wikipedia. 

## Creating Your Own
1. Inside of `/public/decks` you will find a folder called `rider-waite`. You should play with the system a bit to get a sense for how it works, then duplicate or rename this folder to make a template for your own deck.

2. **Card Images**: Put your images in the `images` subfolder. A full tarot deck includes 78 cards as well as an image for the back of the card and an empty frame (80 images total). In spite of its name, CloudTarot is an oracle card system (not limited to Tarot), meaning that you do NOT need to create a full deck to use it. You are free to design your own card systems using any number of cards. The default setup includes a 78 card classic "celtic cross" reading as well as a three and single card draw. All images should be the same dimensions.

3. **Text:**
All of the text for your reading should be put into the `index.json` file in the deck folder. The pattern should be self explanatory, just be sure to [follow JSON rules](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON) for formatting and quotes.

4. **Configuration:**
Configuration options for a reading go in the `config` section. You may have more than one configuration per deck if your deck supports it. The `version` key should be unique for each deck and each version of a deck if you have made major changes.  We are using JWT here to encode the share links (this value is used as a signing key) but this is in no way a security system, so feel free to use any string you like as long as it's unique. [A UUID](https://www.uuidgenerator.net/) is fine. 

4. **Spreads:**  
For each of the named sub-blocks in the `config` section, you should provide a CSS layout describing the position of the cards. 

## Hosting your Cards
This is a complete static site. Run `yarn build` upload the build directory to any static web host. You may consider using [Gitlab](https://about.gitlab.com/product/pages/) or [Github](https://pages.github.com/) pages which are free!

## Running Locally
This is a simple React app building using create-react-app. If you have no idea what that means you probably want to take a look at the [React documentation](https://reactjs.org/). If you have done this before, this is a server-less, standalone app. Download the repo and `yarn install; yarn start` will get you up and running. Alternatively you may use the Docker image for an even faster startup.

## Docker
There is a docker image available as part of this repository. Assuming you have [Docker](https://www.docker.com/) installed, this is the fastest way to get up and running:
`docker run -p 8000:8000 registry.gitlab.com/theplacelab/cloudtarot/client`
